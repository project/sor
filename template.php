<?php
/* THis function is needed for the correct implementaion of the i18n module under drupal 4.7
see http://drupal.org/node/78966 and  http://drupal.org/node/64412 (#14) */
function  _phptemplate_variables($hook, $variables) {
    switch($hook) {
        case 'page' :
     $currentpage = drupal_get_path_alias($_GET['q']);
    $frontpage = variable_get('site_frontpage', 'node');
    if ($currentpage == $frontpage or $currentpage==$GLOBALS['locale'].'/'.$frontpage) {
        $variables['is_front'] = true; }
    break;
  }
  return $variables;
}

/*-- Functions For Drawing the Bordering Table Needed for the Rounded Look --

Two routines which are needed for the theme.
These functions are called from 'php.tpl.php' and are used to build a 3x3 table surrounding the header region as wells as the side bars, central region and the footer.  The 3x3 table is rendered in such a way that the outer 8 cells form an border/edge with rounded corners. The central cell contains the header (and other) block(s)
*/
/*
 $tablespecification is intended to contain id and class statements needed for a particular table
 $basep = $base_path
*/
function roundness_border_part1($tablespecification,$tdir) {
  /*$basep=base_path();*/
  return "<! Start of SOR Border Table surrounding actual display region >\n
  <TABLE border=\"0\" cellpadding=\"0\" cellspacing=\"0\" $tablespecification>
    <tr><td>
        <IMG src=\"/$tdir"."/images/corner_ul.gif\" class=\"image_corner\">
      </td>
      <td class=\"row_height\"></td>
      <td>
        <IMG src=\"/$tdir"."/images/corner_ur.gif\" class=\"image_corner\">
    </td></tr>
    <tr>
      <td class=\"edge_width\"></td>
      <td width=\"100%\">
<! End of section one of SOR Border Table>\n";
}

function roundness_border_part2($tdir) {
  /*$basep=base_path();*/
  return "<! Start of SOR Border Table section two >\n
      </td>
    <td  class=\"edge_width\"></td></tr>
    <tr >
      <td>
        <IMG src=\"/$tdir"."/images/corner_ll.gif\" class=\"image_corner\">
      </td>
      <td class=\"row_height\"></td>
      <td>
        <IMG src=\"/$tdir"."/images/corner_lr.gif\" class=\"image_corner\">
    </td></tr>
  </TABLE>
<! End of SOR Border Table surrounding actual display region >\n";
}
?>