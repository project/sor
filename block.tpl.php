<?php
  /**** Print rounded are only when Block is in sidebar ************/
  if ($block->region == "left" or $block->region == "right")
    print roundness_border_part1("class=\"default_sideblock rounded_spacing block block-".$block->module."\" id=\"block-".$block->module."-".$block->delta."\"",$directory);
?>

<?php
/* If in sidebar region 'div' without class and id arguments, since they are in the surrounding table */
if ($block->region == "left" or $block->region == "right") { ?>
  <div>
<?php } else
/* If NOT in sidebar region normal 'div'*/
{ ?>
  <div class="block block-<?php print $block->module; ?>" id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>">
<?php } ?>

    <h2 class="title"><?php print $block->subject; ?></h2>
    <div class="content"><?php print $block->content; ?></div>
 </div>

<?php
  /**** Print rounded are only when Block is in sidebar ************/
  if ($block->region == "left" or $block->region == "right")
    print roundness_border_part2($directory);
?>
