<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print strip_tags(str_replace("<br>"," ",$head_title)) ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<?php
/***** BEGIN: Roundness Theme Code inserted here ************/
print roundness_border_part1("id=\"header\" class=\"rounded_spacing fullwidth\"",$directory);
/***** END:   Roundness Theme Code inserted here ************/
?>
  <table border="0" cellpadding="0" cellspacing="0" id="header">
    <tr><td id="logo" rowspan="3">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
    </td>
    <td width="40%">
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
    </td>
    <td width="60%">
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </td>
    <td id="menu">
       <?php print $search_box?>
    </td></tr>
    <tr><td colspan="3" id="menu">
      <?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('links', $secondary_links) ?></div><?php } ?>
      <?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links) ?></div><?php } ?>
    </td></tr>
    <tr><td colspan="3"><div><?php print $header ?></div>
    </td></tr>
  </table>
<?php
/***** BEGIN: Roundness Theme Code inserted here ************/
print roundness_border_part2($directory);
/***** END:   Roundness Theme Code inserted here ************/
?>

<table cellpadding="0" cellspacing="0" class="rounded_spacing fullwidth">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) {
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part1("id=\"mission_color\" "."class=\"rounded_spacing\"",$directory);
      /***** END:   Roundness Theme Code inserted here ************/ ?>
      <div id="mission"><?php print $mission ?></div>
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part2($directory);
      /***** END:   Roundness Theme Code inserted here ************/ } ?>
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part1("id=\"cbar-color\"",$directory);
      /***** END:   Roundness Theme Code inserted here ************/
      ?>
        <div id="main">
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
      </div>
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part2($directory);
      /***** END:   Roundness Theme Code inserted here ************/
      ?>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<?php
/***** BEGIN: Roundness Theme Code inserted here ************/
print roundness_border_part1("id=\"footer-color\" class=\"fullwidth\"",$directory);
/***** END:   Roundness Theme Code inserted here ************/
?>
<table border=0 cellpadding=0 cellspacing=0><tr>
<td>
<nobr>Powered by <a href="http://drupal.org"><img src="<?php print $base_path ?>misc/powered-gray-80x15.png"></a></nobr>
</td>
<td id="footer">
  <?php print $footer_message ?>
</td>
</table>
<?php
/***** BEGIN: Roundness Theme Code inserted here ************/
print roundness_border_part2($directory);
/***** END:   Roundness Theme Code inserted here ************/
?>
<?php print $closure ?>
</body>
</html>
